package uex.asee.cajas;

import static org.junit.Assert.assertEquals;

import uex.asee.cajas.Models.Item;

import org.junit.Test;

public class ItemUnitTest {

    @Test
    public void shouldGetImgInfoOfItem() {
        Item item = new Item();
        item.setImginfo("imagen");
        assertEquals("imagen",item.getImginfo());
    }

    @Test
    public void shouldGetIdOfItem() {
        Item item = new Item();
        item.set_id("1");
        assertEquals("1",item.get_id());
    }

    @Test
    public void shouldGetNameOfItem() {
        Item item = new Item();
        item.setName("item");
        assertEquals("item",item.getName());
    }

    @Test
    public void shouldGetIdCajaOfItem() {
        Item item = new Item();
        item.setIdcaja("1");
        assertEquals("1",item.getIdcaja());
    }

    @Test
    public void shouldGetDescriptionOfItem() {
        Item item = new Item();
        item.setDescription("desc");
        assertEquals("desc",item.getDescription());
    }

    @Test
    public void shouldGetUserIdOfItem() {
        Item item = new Item();
        item.setUserid("1");
        assertEquals("1",item.getUserid());
    }

    @Test
    public void shouldGetCategoriaOfItem() {
        Item item = new Item();
        item.setCategoria("categ");
        assertEquals("categ",item.getCategoria());
    }

}
