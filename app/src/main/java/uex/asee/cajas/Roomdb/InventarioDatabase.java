package uex.asee.cajas.Roomdb;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import uex.asee.cajas.Entidades.Caja;
import uex.asee.cajas.Entidades.Casa;
import uex.asee.cajas.Entidades.Objeto;
import uex.asee.cajas.Entidades.User;

@Database(entities = {Objeto.class, Caja.class, Casa.class, User.class}, version = 1)
public abstract class InventarioDatabase extends RoomDatabase {
    public static InventarioDatabase instance;

    public static InventarioDatabase getInstance(Context context) {
        if (instance == null)
            instance = Room.databaseBuilder(context, InventarioDatabase.class, "inventario.db").build();
        return instance;
    }

    public  abstract  ObjetoDAO getDaoObjeto();

    public  abstract  CajaDAO getDaoCaja();

    public  abstract  CasaDAO getDaoCasa();

    public abstract UserDAO getDaoUser();

}