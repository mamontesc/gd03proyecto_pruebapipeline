package uex.asee.cajas.Roomdb;

import static androidx.room.OnConflictStrategy.REPLACE;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import uex.asee.cajas.Entidades.Caja;
import uex.asee.cajas.Entidades.Casa;

import java.util.List;

@Dao
public interface CajaDAO {
    @Query("SELECT * FROM caja")
    public LiveData<List<Caja>> getAll();

    @Insert
    public void insert(Caja caja);

    @Query("DELETE FROM caja")
    public void deleteAll();

    @Query("DELETE FROM caja WHERE ID = :id")
    public void deleteCajaById(String id);

    @Insert(onConflict = REPLACE)
    public void bulkInsert(List<Caja> cajas);

    @Update
    public void update(Caja caja);

    @Query("SELECT * FROM caja WHERE ID = :id")
    public Caja getCajaById(String id);

    @Query("SELECT * FROM caja WHERE idCasa = :id")
    public List<Caja> getAllByCasa(String id);
}