package uex.asee.cajas.remote.Objetos;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;


import uex.asee.cajas.Entidades.Objeto;
import uex.asee.cajas.remote.AppExecutors;


public class ItemsNetworkDataSource {
    private static final String LOG_TAG = ItemsNetworkDataSource.class.getSimpleName();
    private static ItemsNetworkDataSource sInstance;

    // LiveData storing the latest downloaded weather forecasts
    private final MutableLiveData<List<Objeto>> mDownloadedObjetos;

    private ItemsNetworkDataSource() {
        mDownloadedObjetos = new MutableLiveData<>();
    }

    public synchronized static ItemsNetworkDataSource getInstance() {
        Log.d(LOG_TAG, "Getting the network data source");
        if (sInstance == null) {
            sInstance = new ItemsNetworkDataSource();
            Log.d(LOG_TAG, "Made new network data source");
        }
        return sInstance;
    }

    public LiveData<List<Objeto>> getCurrentItems() {
        return mDownloadedObjetos;
    }

    /**
     * Gets the newest repos
     */
    public void fetchItems()  {
        Log.d(LOG_TAG, "Fetch repos started");
        // Get gata from network and pass it to LiveData
        AppExecutors.getInstance().networkIO().execute(new ItemsNetworkRunnable(mDownloadedObjetos::postValue));

        if (mDownloadedObjetos.getValue() != null ){
            Log.e("Numero de DataSource", String.valueOf(mDownloadedObjetos.getValue().size()));
        }

    }

    public void setFromDatabase(List<Objeto> objetos){
        mDownloadedObjetos.postValue(objetos);
    }
}
