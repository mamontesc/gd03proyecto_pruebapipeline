package uex.asee.cajas.remote.Casas;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import uex.asee.cajas.Actividades.Houses;
import uex.asee.cajas.Actividades.Login;
import uex.asee.cajas.Adapters.OnHousesLoaderListener;
import uex.asee.cajas.Entidades.Casa;
import uex.asee.cajas.Interfaces.HouseAPI;
import uex.asee.cajas.Models.House;
import uex.asee.cajas.Roomdb.InventarioDatabase;
import uex.asee.cajas.remote.AppExecutors;
import uex.asee.cajas.remote.RetrofitClient;

public class HousesNetworkRunnable implements Runnable {
    private final OnHousesLoaderListener mOnHousesLoadedListener;


    public HousesNetworkRunnable(OnHousesLoaderListener onHousesLoadedListener){
        mOnHousesLoadedListener = onHousesLoadedListener;

    }

    @Override
    public void run() {
        HouseAPI houseAPI = RetrofitClient.getClient().create(HouseAPI.class);
        Call<List<House>> call = houseAPI.getHouses(Login.TOKEN.getToken());
        final List<Casa> houses =  new ArrayList<>() ;
        call.enqueue(new Callback<List<House>>() {
            @Override
            public void onResponse(Call<List<House>> call, Response<List<House>> response) {
                if(response.isSuccessful()){
                    for(House h:response.body()){
                        houses.add(new Casa(h.get_id(),h.getName(),h.getDescription()));
                    }
                    System.out.println("LOS ITEMS SON DE TAMAÑO "+houses.size()+"---------------------------------------------------------");
                    AppExecutors.getInstance().mainThread().execute(() -> mOnHousesLoadedListener.onHousesLoaded(houses));
                }


            }
            @Override
            public void onFailure(Call<List<House>> call, Throwable t) {

            }
        });


    }
}
