package uex.asee.cajas.remote.Objetos;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uex.asee.cajas.Actividades.Login;
import uex.asee.cajas.Adapters.OnHousesLoaderListener;
import uex.asee.cajas.Adapters.OnItemsLoaderListener;
import uex.asee.cajas.Entidades.Casa;
import uex.asee.cajas.Entidades.Objeto;
import uex.asee.cajas.Interfaces.HouseAPI;
import uex.asee.cajas.Interfaces.ItemAPI;
import uex.asee.cajas.Models.House;
import uex.asee.cajas.Models.Item;
import uex.asee.cajas.remote.AppExecutors;
import uex.asee.cajas.remote.RetrofitClient;

public class ItemsNetworkRunnable implements Runnable{
    private final OnItemsLoaderListener mOnItemsLoaderListener;


    public ItemsNetworkRunnable(OnItemsLoaderListener onItemsLoaderListener){
        mOnItemsLoaderListener = onItemsLoaderListener;

    }

    @Override
    public void run() {
        ItemAPI itemAPI = RetrofitClient.getClient().create(ItemAPI.class);
        Call<List<Item>> call = itemAPI.getItems(Login.TOKEN.getToken());
        final List<Objeto> items =  new ArrayList<>() ;
        call.enqueue(new Callback<List<Item>>() {
            @Override
            public void onResponse(Call<List<Item>> call, Response<List<Item>> response) {
                if(response.isSuccessful()){
                    for(Item item:response.body()){
                        items.add(new Objeto(item.get_id(),item.getName(),item.getCategoria(), item.getImginfo(), item.getDescription(),item.getIdcaja(),item.getUserid()));

                    }
                    System.out.println("LOS ITEMS SON DE TAMAÑO "+items.size()+"---------------------------------------------------------");
                    AppExecutors.getInstance().mainThread().execute(() -> mOnItemsLoaderListener.onItemsLoaded(items));
                }
            }
            @Override
            public void onFailure(Call<List<Item>> call, Throwable t) {


            }
        });


    }
}
