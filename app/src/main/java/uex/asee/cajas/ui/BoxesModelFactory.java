package uex.asee.cajas.ui;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import uex.asee.cajas.remote.Cajas.BoxesRepository;
import uex.asee.cajas.remote.Casas.HousesRepository;

public class BoxesModelFactory extends ViewModelProvider.NewInstanceFactory{
    private final BoxesRepository mBoxesRepository;

    public BoxesModelFactory(BoxesRepository boxesRepository) {
        this.mBoxesRepository = boxesRepository;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        //noinspection unchecked
        //1-Se devuelve el objeto de tipo mainActivityViewModel con la dependencia inyectada del repositorio
        return (T) new BoxesViewModel(mBoxesRepository);
    }
}
