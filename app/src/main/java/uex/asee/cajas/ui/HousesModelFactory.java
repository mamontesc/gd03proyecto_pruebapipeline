package uex.asee.cajas.ui;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import uex.asee.cajas.remote.Casas.HousesRepository;

public class HousesModelFactory extends ViewModelProvider.NewInstanceFactory {
    private final HousesRepository mHousesRepository;

    public HousesModelFactory(HousesRepository housesRepository) {
        this.mHousesRepository = housesRepository;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        //noinspection unchecked
        //1-Se devuelve el objeto de tipo mainActivityViewModel con la dependencia inyectada del repositorio
        return (T) new HousesViewModel(mHousesRepository);
    }
}
