package uex.asee.cajas.ui;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import uex.asee.cajas.Entidades.Caja;
import uex.asee.cajas.Entidades.Casa;
import uex.asee.cajas.remote.Cajas.BoxesRepository;
import uex.asee.cajas.remote.Casas.HousesRepository;

public class BoxesViewModel extends ViewModel {

    private final BoxesRepository mBoxesRepository; //1-Depende del repositorio
    private final LiveData<List<Caja>> mBoxes; //2-Guarda un livedata de una lista de repositorios (la funcion de la app es mostrar la lista)
    //4-Se inyecta la dependencia del repositorio en el constructor

    public BoxesViewModel(BoxesRepository repository) {
        mBoxesRepository = repository;
        mBoxes = mBoxesRepository.getCurrentBoxes();


    }

    public LiveData<List<Caja>>  randomBox(){
        MutableLiveData<List<Caja>> rboxes = new MutableLiveData<>();
        List<Caja> aux = new ArrayList<Caja>();
        if (mBoxes.getValue() != null) {
            int randomNum = ThreadLocalRandom.current().nextInt(0, mBoxes.getValue().size());
            aux.add(mBoxes.getValue().get(randomNum));
            rboxes.postValue(aux);
        }
        return rboxes;
    }
    public void onRefresh() {  mBoxesRepository.onRefresh(); }

    public LiveData<List<Caja>> getBoxes() {
        return mBoxes;
    }

    public LiveData<List<Caja>> getCasasById(String id) {

        MutableLiveData<List<Caja>> rboxes = new MutableLiveData<>();
        List<Caja> aux = new ArrayList<Caja>();
        List<Caja>aux1 = mBoxes.getValue();
        if (mBoxes.getValue() != null){
            for (Caja c : aux1) {
                System.out.println(id);
                if (c.getIdCasa() != null) {
                    if (c.getIdCasa().equals(id)) {
                        aux.add(c);
                        System.out.println("Entra" + aux.size());
                    }
                }
            }
            rboxes.postValue(aux);
      }
        return rboxes;
    }
}
