package uex.asee.cajas.ui;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import uex.asee.cajas.remote.Objetos.ItemsRepository;

public class ItemsModelFactory extends ViewModelProvider.NewInstanceFactory{
    private final ItemsRepository mItemsRepository;

    public ItemsModelFactory(ItemsRepository itemsRepository) {
        this.mItemsRepository = itemsRepository;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        //noinspection unchecked
        //1-Se devuelve el objeto de tipo mainActivityViewModel con la dependencia inyectada del repositorio
        return (T) new ItemsViewModel(mItemsRepository);
    }
}
