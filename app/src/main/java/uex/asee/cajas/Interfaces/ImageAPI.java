package uex.asee.cajas.Interfaces;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Streaming;

public interface ImageAPI {
    @Streaming
    @GET("image")
    Call<String> downloadFileWithDynamicUrlSync(@Query("imgsrc") String fileUrl);
}
