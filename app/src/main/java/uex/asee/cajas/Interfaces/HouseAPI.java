package uex.asee.cajas.Interfaces;

import uex.asee.cajas.Models.House;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;

public interface HouseAPI {
    @POST("casa")
    @FormUrlEncoded
    public Call<House> createHouse(@Header("auth-token") String authtoken,
                                   @Field("name") String name,
                                   @Field("description") String description);

    @GET("casa")
    public Call<List<House>> getHouses(@Header("auth-token") String authtoken);

    @GET("casa/id")
    public Call<House> getHouse(@Header("auth-token") String authtoken, @Query("id") String id);

    @DELETE("casa")
    Call <ResponseBody> deleteHouse(@Header("auth-token")String authtoken,@Query("id") String id);

    @PUT("casa")
    @FormUrlEncoded
    Call<ResponseBody> updateHouse(@Header("auth-token")String authtoken,@Query("id") String id,
                                   @Field("name") String name,
                                   @Field("description") String description);
}
