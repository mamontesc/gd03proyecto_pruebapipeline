package uex.asee.cajas.Actividades;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import uex.asee.cajas.Interfaces.UserAPI;
import uex.asee.cajas.Models.Token;
import uex.asee.cajas.R;
import uex.asee.cajas.remote.RetrofitClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Login extends AppCompatActivity {

    public static Token TOKEN = new Token();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        final EditText userText = (EditText) findViewById(R.id.username);
        final EditText passwordText = (EditText) findViewById(R.id.password);

        Button button = (Button) findViewById(R.id.loginButton);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!userText.getText().toString().isEmpty() && !passwordText.getText().toString().isEmpty()) {
                    LoginUser(userText.getText().toString(), passwordText.getText().toString());
                } else {
                    Toast.makeText(Login.this, "Has dejado campos vacios", Toast.LENGTH_SHORT).show();
                }
            }
        });

        Button regbutton = (Button) findViewById(R.id.nRegButton);
        regbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getBaseContext(), Register.class);
                startActivity(intent);
            }
        });
    }

    public void LoginUser(String email, String password) {
        UserAPI userAPI = RetrofitClient.getClient().create(UserAPI.class);
        Call<Token> call = userAPI.loginUser(email, password);

        call.enqueue(new Callback<Token>() {
            @Override
            public void onResponse(Call<Token> call, Response<Token> response) {
                if (response.isSuccessful()) {
                    TOKEN = response.body();
                    System.out.println(TOKEN.getToken());
                    Intent intent = new Intent(getBaseContext(), MainActivity.class);
                    startActivity(intent);
                } else {
                    JSONObject jObjError = null;
                    try {
                        jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(Login.this, jObjError.getString("msg"), Toast.LENGTH_SHORT).show();
                    } catch (JSONException e) {
                        Toast.makeText(Login.this, "Error interno", Toast.LENGTH_SHORT).show();
                    } catch (IOException e) {
                        Toast.makeText(Login.this, "Error interno", Toast.LENGTH_SHORT).show();
                    }
                    return;
                }
            }

            @Override
            public void onFailure(Call<Token> call, Throwable t) {
                Toast.makeText(Login.this, "Error al conectarse con los servicios", Toast.LENGTH_SHORT).show();
            }
        });
    }
}