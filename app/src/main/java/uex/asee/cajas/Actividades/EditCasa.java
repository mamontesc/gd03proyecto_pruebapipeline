package uex.asee.cajas.Actividades;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import uex.asee.cajas.remote.AppExecutors;
import uex.asee.cajas.Entidades.Casa;
import uex.asee.cajas.Interfaces.HouseAPI;
import uex.asee.cajas.R;
import uex.asee.cajas.Roomdb.InventarioDatabase;
import uex.asee.cajas.remote.RetrofitClient;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditCasa extends AppCompatActivity {

    public static final String HOUSE_EDIT_ID = "HOUSE_EDIT_ID";
    private Casa casaDetallada;
    private EditText nameCasa;
    private EditText descCasa;
    String house_id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_casa);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
        nameCasa = (EditText) findViewById(R.id.ecasa_name);
        descCasa = (EditText) findViewById(R.id.ecasa_desc);
        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                InventarioDatabase database = InventarioDatabase.getInstance(EditCasa.this);
                house_id = getIntent().getStringExtra(HOUSE_EDIT_ID);
                casaDetallada = database.getDaoCasa().getCasaById(house_id);
                nameCasa.setText(casaDetallada.getName());
                descCasa.setText(casaDetallada.getDescription());

            }
        });

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.create_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        final EditText nCajaName = (EditText) findViewById(R.id.ecasa_name);
        switch (item.getItemId()) {
            case R.id.action_cancel:
                this.finish();
                return true;
            case R.id.action_confirm:
                String name = nameCasa.getText().toString();
                String description = descCasa.getText().toString();
                if(name.isEmpty() || description.isEmpty()){
                    Toast.makeText(EditCasa.this, "No dejes campos vacios", Toast.LENGTH_SHORT).show();
                }else {
                    updateHouse(name,description);
                    this.finish();
                    return true;
                }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void updateHouse(String name,String description){
        HouseAPI houseAPI = RetrofitClient.getClient().create(HouseAPI.class);
        Call<ResponseBody> call = houseAPI.updateHouse(Login.TOKEN.getToken(),house_id,name,description);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.isSuccessful()){
                    AppExecutors.getInstance().diskIO().execute(() -> {
                        InventarioDatabase database = InventarioDatabase.getInstance(EditCasa.this);
                        casaDetallada.setName(nameCasa.getText().toString());
                        casaDetallada.setDescription(descCasa.getText().toString());
                        database.getDaoCasa().update(casaDetallada);
                    });
                }else{
                    Toast.makeText(EditCasa.this, "Error al actualizar la casa", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(EditCasa.this, "Error al conectarse con los servicios", Toast.LENGTH_SHORT).show();
            }
        });
    }
}