package uex.asee.cajas.Actividades;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import uex.asee.cajas.AppContainer;
import uex.asee.cajas.MyApplication;
import uex.asee.cajas.remote.AppExecutors;
import uex.asee.cajas.Entidades.Casa;
import uex.asee.cajas.Adapters.HouseAdapter;
import uex.asee.cajas.Interfaces.HouseAPI;
import uex.asee.cajas.Models.House;
import uex.asee.cajas.R;
import uex.asee.cajas.Roomdb.InventarioDatabase;
import uex.asee.cajas.remote.RetrofitClient;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uex.asee.cajas.ui.HousesViewModel;

public class Houses extends AppCompatActivity {

    RecyclerView recView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_houses);
    }
    @Override
    protected void onStart(){
        super.onStart();
        setContentView(R.layout.activity_houses);


        recView = findViewById(R.id.recy);
        recView.setHasFixedSize(true);
        recView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recView.addItemDecoration(new DividerItemDecoration(recView.getContext(), DividerItemDecoration.VERTICAL));
        HouseAdapter houseAdapter = new HouseAdapter(Houses.this, new ArrayList<>(), casa -> {
            Intent intent = new Intent(getApplicationContext(), CasaDetail.class);
            intent.putExtra(CasaDetail.HOUSE_ID, casa.getId());
            startActivity(intent);
        });

        AppContainer appContainer = MainActivity.appContainer;

        HousesViewModel mViewModel = new ViewModelProvider(this, appContainer.housesfactory).get(HousesViewModel.class);

        mViewModel.getCasas().observe(this, casas -> {
            houseAdapter.swap(casas);
        });

        recView.setAdapter(houseAdapter);
        Button addcasa = findViewById(R.id.addcasa);
        addcasa.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), NewHouse.class);
                startActivity(intent);
            }
        });


    }

}