package uex.asee.cajas.Actividades;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import uex.asee.cajas.AppContainer;
import uex.asee.cajas.remote.AppExecutors;
import uex.asee.cajas.Entidades.Objeto;
import uex.asee.cajas.Interfaces.ItemAPI;
import uex.asee.cajas.Adapters.ItemAdapter;
import uex.asee.cajas.R;
import uex.asee.cajas.Roomdb.InventarioDatabase;
import uex.asee.cajas.remote.RetrofitClient;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uex.asee.cajas.ui.ItemsViewModel;

public class deleteItem extends AppCompatActivity {
    private boolean isFABOpen = false;
    private List<Objeto> objetos;
    private List<Objeto> seleccionados = new ArrayList<Objeto>();
    GridView gridView;
    ItemAdapter itemAdapter ;
    ItemsViewModel mViewModel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete_item);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
        gridView = (GridView) findViewById(R.id.griddi);

        itemAdapter = new ItemAdapter(this, new ArrayList<>(), (item,view) -> {
            if (!seleccionados.contains(item)) {
                seleccionados.add(item);
                view.setBackgroundColor(getResources().getColor(R.color.black));
            }
            else {
                seleccionados.remove(item);
                view.setBackgroundColor(0);
            }
        });

        AppContainer appContainer = MainActivity.appContainer;

        mViewModel = new ViewModelProvider(this, appContainer.itemsfactory).get(ItemsViewModel.class);

        mViewModel.getItems().observe(this, objetos -> {
            itemAdapter.swap(objetos);
        });


        gridView.setAdapter(itemAdapter);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.create_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_cancel:
                this.finish();
                return true;
            case R.id.action_confirm:
                for (Objeto obj : seleccionados) {
                    deleteItem(obj.getId());
                }
                this.finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void deleteItem(String id){
        ItemAPI itemAPI = RetrofitClient.getClient().create(ItemAPI.class);
        Call<ResponseBody> call = itemAPI.deleteItem(Login.TOKEN.getToken(),id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.isSuccessful()){
                    AppExecutors.getInstance().diskIO().execute(() -> {
                        InventarioDatabase database = InventarioDatabase.getInstance(deleteItem.this);
                        database.getDaoObjeto().deleteObjetoById(id);
                    });
                }else{
                    Toast.makeText(deleteItem.this, "Ha ocurrido un error al borrar el item con id: "+id, Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(deleteItem.this, "Error al conectarse con los servicios", Toast.LENGTH_SHORT).show();
            }
        });
    }
}