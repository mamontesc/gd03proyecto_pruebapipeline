package uex.asee.cajas.Actividades;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProvider;

import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationBarView;

import uex.asee.cajas.AppContainer;
import uex.asee.cajas.remote.AppExecutors;
import uex.asee.cajas.Entidades.Caja;
import uex.asee.cajas.Entidades.Casa;
import uex.asee.cajas.Interfaces.BoxAPI;
import uex.asee.cajas.Models.Box;
import uex.asee.cajas.R;
import uex.asee.cajas.Roomdb.InventarioDatabase;
import uex.asee.cajas.remote.RetrofitClient;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uex.asee.cajas.ui.HousesViewModel;

public class NewBox extends AppCompatActivity {
    Spinner spinner;
    ArrayList<String> casasNames= new ArrayList<String>();
    ArrayList<Casa> houses = new ArrayList<>();
    EditText nBoxName;
    EditText nBoxDesc;
    ImageView imageView;
    String idCasa;
    Uri auxuri;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_box);
        nBoxName = (EditText) findViewById(R.id.nbox_name);
        nBoxDesc = (EditText) findViewById(R.id.nbox_desc);
        spinner = findViewById(R.id.spinner);
        imageView = findViewById(R.id.prev_new_box_image);
        ActivityResultLauncher<String> mGetContent = registerForActivityResult(new ActivityResultContracts.GetContent(),
                uri -> {
                    System.out.println(uri);
                    imageView.setImageURI(uri);
                    auxuri = uri;
                });
        Button btnChooseFile = findViewById(R.id.button_new_box_select_photo);
        btnChooseFile.setOnClickListener(view -> {
            mGetContent.launch("image/*");
        });

        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);

        AppContainer appContainer = MainActivity.appContainer;

        HousesViewModel mViewModel = new ViewModelProvider(this, appContainer.housesfactory).get(HousesViewModel.class);

        mViewModel.getCasas().observe(this, casas -> {

            if (casas != null) {
                houses = (ArrayList<Casa>) casas;
                System.out.println("TAMA´ÑO CASAS " + casas.size());
                for (Casa house : casas) {
                    casasNames.add(house.getName());

                }
                System.out.println("TAMAÑO CASASNOM " + casasNames.size());
            }
            ArrayAdapter<String> adapter2=new ArrayAdapter<String>(getApplicationContext(),android.R.layout.simple_spinner_item, casasNames);
            spinner.setAdapter(adapter2);
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (spinner.getSelectedItemPosition() > -1){
                        idCasa = houses.get(spinner.getSelectedItemPosition()).getId();
                    System.out.println("HOLAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
                }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        });



    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.create_menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final EditText nBoxName = (EditText) findViewById(R.id.nbox_name);
        switch (item.getItemId()) {
            case R.id.action_cancel:
                this.finish();
                return true;
            case R.id.action_confirm:
                System.out.println("valor spinner " + spinner.getSelectedItem() + " posicion " +spinner.getSelectedItemPosition());

                System.out.println(" id casa " + idCasa);
                if(!nBoxName.getText().toString().isEmpty() && !nBoxDesc.getText().toString().isEmpty()){
                    createBox(nBoxName.getText().toString(),nBoxDesc.getText().toString(),idCasa);
                    this.finish();
                    return true;
                }else{
                    Toast.makeText(NewBox.this, "Por favor rellena todos los campos", Toast.LENGTH_SHORT).show();
                }
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    public void createBox(String name,String description,String idcasa){

        BoxAPI houseAPI = RetrofitClient.getClient().create(BoxAPI.class);
        RequestBody rname = RequestBody.create(MediaType.parse("text/plain"),name);
        RequestBody rdescription = RequestBody.create(MediaType.parse("text/plain"),description);
        RequestBody ridcasa = null;
        if(idcasa != null){
            ridcasa = RequestBody.create(MediaType.parse("text/plain"),idcasa);
        }
        RequestBody rimg = null;
        MultipartBody.Part rbimg = null;
        try {
            if(auxuri !=  null){
                InputStream is = getContentResolver().openInputStream(auxuri);
                if(is!= null){
                    rimg = RequestBody.create(
                            MediaType.parse(getContentResolver().getType(auxuri)),
                            getBytes(is)

                    );
                    MimeTypeMap mime = MimeTypeMap.getSingleton();
                    String type = mime.getExtensionFromMimeType(getContentResolver().getType(auxuri));

                    rbimg =  MultipartBody.Part.createFormData("image", name+"."+type, rimg);
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Call<Box> call = houseAPI.createBox(Login.TOKEN.getToken(),rname,ridcasa,rbimg,rdescription);

        call.enqueue(new Callback<Box>() {
            @Override
            public void onResponse(Call<Box> call, Response<Box> response) {
                if(response.isSuccessful()){
                    String id = response.body().get_id();
                    String name =  response.body().getName();
                    String idCasa =  response.body().getIdcasa();
                    String description = response.body().getDescription();
                    String userId = response.body().getUserid();
                    String imagen = response.body().getImginfo();
                    if(imagen == null){
                        imagen = "";
                    }
                    if(idCasa == null){
                        idCasa = "";
                    }
                    if(description == null){
                        description = "";
                    }
                    Caja caja = new Caja(id,name,idCasa,description,userId,imagen);
                    AppExecutors.getInstance().diskIO().execute(new Runnable() {
                        @Override
                        public void run() {
                            InventarioDatabase database = InventarioDatabase.getInstance(NewBox.this);
                            database.getDaoCaja().insert(caja);
                        }
                    });
                }else{
                    Toast.makeText(NewBox.this, "Error al crear caja", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<Box> call, Throwable t) {
                Toast.makeText(NewBox.this, "Error al conectarse con los servicios", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public byte[] getBytes(InputStream is) throws IOException {
        ByteArrayOutputStream byteBuff = new ByteArrayOutputStream();

        int buffSize = 1024;
        byte[] buff = new byte[buffSize];

        int len = 0;
        while ((len = is.read(buff)) != -1) {
            byteBuff.write(buff, 0, len);
        }

        return byteBuff.toByteArray();
    }

}