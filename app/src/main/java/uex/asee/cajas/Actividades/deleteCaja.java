package uex.asee.cajas.Actividades;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import uex.asee.cajas.Adapters.ItemAdapter;
import uex.asee.cajas.AppContainer;
import uex.asee.cajas.remote.AppExecutors;
import uex.asee.cajas.Entidades.Caja;
import uex.asee.cajas.Interfaces.BoxAPI;
import uex.asee.cajas.Adapters.ItemAdapterCaja;
import uex.asee.cajas.R;
import uex.asee.cajas.Roomdb.InventarioDatabase;
import uex.asee.cajas.remote.RetrofitClient;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uex.asee.cajas.ui.BoxesViewModel;

public class deleteCaja extends AppCompatActivity {
    private boolean isFABOpen = false;
    private List<Caja> cajas;
    private List<Caja> seleccionadas = new ArrayList<Caja>();
    GridView gridView;
    ItemAdapterCaja itemAdapterCaja ;
    BoxesViewModel mViewModel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete_caja);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
        gridView = (GridView) findViewById(R.id.griddc);

        itemAdapterCaja = new ItemAdapterCaja(this, new ArrayList<>(), (caja, view) -> {
            if (!seleccionadas.contains(caja)) {
                seleccionadas.add(caja);
                view.setBackgroundColor(getResources().getColor(R.color.black));
            }
            else {
                seleccionadas.remove(caja);
                view.setBackgroundColor(0);
            }
        });

        AppContainer appContainer = MainActivity.appContainer;

        mViewModel = new ViewModelProvider(this, appContainer.boxesfactory).get(BoxesViewModel.class);

        mViewModel.getBoxes().observe(this, boxes -> {
            itemAdapterCaja.swap(boxes);
        });



        gridView.setAdapter(itemAdapterCaja);

            }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.create_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_cancel:
                this.finish();
                return true;
            case R.id.action_confirm:
                for (Caja box : seleccionadas) {
                    deleteCaja(box.getId());
                };
                this.finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void deleteCaja(String id){
        BoxAPI boxAPI = RetrofitClient.getClient().create(BoxAPI.class);
        Call<ResponseBody> call = boxAPI.deleteBox(Login.TOKEN.getToken(),id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.isSuccessful()){
                    AppExecutors.getInstance().diskIO().execute(() -> {
                        InventarioDatabase database = InventarioDatabase.getInstance(deleteCaja.this);
                        database.getDaoCaja().deleteCajaById(id);
                    });
                }else{
                    Toast.makeText(deleteCaja.this, "Ha ocurrido un error al borrar la caja con id: "+id, Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(deleteCaja.this, "Error al conectarse con los servicios", Toast.LENGTH_SHORT).show();
            }
        });
    }


}