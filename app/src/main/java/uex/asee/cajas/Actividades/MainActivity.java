package uex.asee.cajas.Actividades;

import android.content.DialogInterface;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationBarView;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uex.asee.cajas.AppContainer;
import uex.asee.cajas.remote.AppExecutors;
import uex.asee.cajas.Fragmentos.BoxesFragment;
import uex.asee.cajas.Fragmentos.BuscarFragment;
import uex.asee.cajas.Entidades.Caja;
import uex.asee.cajas.Entidades.Casa;
import uex.asee.cajas.Entidades.Objeto;
import uex.asee.cajas.Fragmentos.HomeFragment;
import uex.asee.cajas.Interfaces.BoxAPI;
import uex.asee.cajas.Interfaces.HouseAPI;
import uex.asee.cajas.Interfaces.ItemAPI;
import uex.asee.cajas.Interfaces.UserAPI;
import uex.asee.cajas.Fragmentos.ItemsFragment;
import uex.asee.cajas.Models.Box;
import uex.asee.cajas.Models.House;
import uex.asee.cajas.Models.Item;
import uex.asee.cajas.R;
import uex.asee.cajas.Roomdb.InventarioDatabase;
import uex.asee.cajas.remote.RetrofitClient;

public class MainActivity extends AppCompatActivity{
    Fragment frag = null;
    public static AppContainer appContainer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SyncDatabase();

        appContainer = new AppContainer(this);

        BottomNavigationView bottomNav = (BottomNavigationView) findViewById(R.id.bottomNavigationView);

        LoadLayout(new HomeFragment());

        bottomNav.setOnItemSelectedListener(new NavigationBarView.OnItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();

                switch (id) {
                    case R.id.home:
                        frag = new HomeFragment();
                        break;
                    case R.id.search:
                        frag = new BuscarFragment();
                        break;
                    case R.id.boxes:
                        frag = new BoxesFragment();
                        break;
                    case R.id.items:
                        frag = new ItemsFragment();
                        break;
                }
                return LoadLayout(frag);
            }
        });

    }

    @Override
    public void onBackPressed(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("¿Estas seguro de que quieres cerrar sesión?")
                .setCancelable(false)
                .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        LogoutUser();
                        finish();
                        MainActivity.super.onBackPressed();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public void LogoutUser(){
        UserAPI userAPI = RetrofitClient.getClient().create(UserAPI.class);
        System.out.println(Login.TOKEN.getToken());
        Call<String> call = userAPI.logoutUser(Login.TOKEN.getToken());
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
            }
        });
    }

    public void SyncDatabase(){
        HouseAPI houseAPI = RetrofitClient.getClient().create(HouseAPI.class);
        BoxAPI boxAPI = RetrofitClient.getClient().create(BoxAPI.class);
        ItemAPI itemAPI = RetrofitClient.getClient().create(ItemAPI.class);

        Call<List<House>> housecall = houseAPI.getHouses(Login.TOKEN.getToken());
        Call<List<Box>> boxcall = boxAPI.getBoxes(Login.TOKEN.getToken());
        Call<List<Item>> itemcall = itemAPI.getItems(Login.TOKEN.getToken());

        housecall.enqueue(new Callback<List<House>>() {
            @Override
            public void onResponse(Call<List<House>> call, Response<List<House>> response) {
                if(response.isSuccessful()){
                    AppExecutors.getInstance().diskIO().execute(new Runnable() {
                        @Override
                        public void run() {
                            InventarioDatabase database = InventarioDatabase.getInstance(MainActivity.this);
                            int i = 0;
                            for(House house:response.body()){
                                if(database.getDaoCasa().getCasaById(house.get_id()) == null){
                                    Casa c = new Casa(house.get_id(),house.getName(),house.getDescription());
                                    database.getDaoCasa().insert(c);
                                    i++;
                                }
                            }
                            final int synced = i;
                            MainActivity.this.runOnUiThread(()-> Toast.makeText(MainActivity.this, "Sincronizadas "+synced+" casas", Toast.LENGTH_SHORT).show());

                        }
                    });
                }else{
                    MainActivity.this.runOnUiThread(()->Toast.makeText(MainActivity.this, "Error al sincronizar los datos de las casas", Toast.LENGTH_SHORT).show());
                }
            }

            @Override
            public void onFailure(Call<List<House>> call, Throwable t) {
                MainActivity.this.runOnUiThread(()->Toast.makeText(MainActivity.this, "Error al sincronizar los datos de las casas", Toast.LENGTH_SHORT).show());
            }
        });

        boxcall.enqueue(new Callback<List<Box>>() {
            @Override
            public void onResponse(Call<List<Box>> call, Response<List<Box>> response) {
                if(response.isSuccessful()){
                    AppExecutors.getInstance().diskIO().execute(new Runnable() {
                        @Override
                        public void run() {
                            InventarioDatabase database = InventarioDatabase.getInstance(MainActivity.this);
                            int i = 0;
                            for(Box box:response.body()){
                                if(database.getDaoCaja().getCajaById(box.get_id()) == null){
                                    String id = box.get_id();
                                    String name =  box.getName();
                                    String idCasa =  box.getIdcasa();
                                    String description = box.getDescription();
                                    String userId = box.getUserid();
                                    String imagen = box.getImginfo();
                                    if(imagen == null){
                                        imagen = "";
                                    }
                                    if(idCasa == null){
                                        idCasa = "";
                                    }
                                    if(description == null){
                                        description = "";
                                    }
                                    Caja caja = new Caja(id,name,idCasa,description,userId,imagen);
                                    database.getDaoCaja().insert(caja);
                                    i++;
                                }
                            }
                            final int synced = i;
                            MainActivity.this.runOnUiThread(()->Toast.makeText(MainActivity.this, "Sincronizadas "+synced+" cajas", Toast.LENGTH_SHORT).show());
                        }
                    });
                }else{
                    MainActivity.this.runOnUiThread(()->Toast.makeText(MainActivity.this, "Error al sincronizar los datos de las cajas", Toast.LENGTH_SHORT).show());
                }
            }

            @Override
            public void onFailure(Call<List<Box>> call, Throwable t) {
                MainActivity.this.runOnUiThread(()->Toast.makeText(MainActivity.this, "Error al sincronizar los datos de las cajas", Toast.LENGTH_SHORT).show());
            }
        });

        itemcall.enqueue(new Callback<List<Item>>() {
            @Override
            public void onResponse(Call<List<Item>> call, Response<List<Item>> response) {
                if(response.isSuccessful()){
                    AppExecutors.getInstance().diskIO().execute(new Runnable() {
                        @Override
                        public void run() {
                            InventarioDatabase database = InventarioDatabase.getInstance(MainActivity.this);
                            int i = 0;
                            for(Item item:response.body()){
                                if(database.getDaoObjeto().getObjetoById(item.get_id()) == null){
                                    String id = item.get_id();
                                    String name =  item.getName();
                                    String idCaja =  item.getIdcaja();
                                    String description = item.getDescription();
                                    String userId = item.getUserid();
                                    String imagen = item.getImginfo();
                                    String categoria = item.getCategoria();
                                    if(imagen == null){
                                        imagen = "";
                                    }
                                    if(idCaja == null){
                                        idCaja = "";
                                    }
                                    if(description == null){
                                        description = "";
                                    }
                                    Objeto it = new Objeto(id,name,categoria,imagen,description,idCaja,userId);
                                    database.getDaoObjeto().insert(it);
                                    i++;
                                }
                            }
                            final int synced = i;
                            MainActivity.this.runOnUiThread(()->Toast.makeText(MainActivity.this, "Sincronizados "+synced+" objetos", Toast.LENGTH_SHORT).show());
                        }
                    });
                }else{
                    MainActivity.this.runOnUiThread(()->Toast.makeText(MainActivity.this, "Error al sincronizar los datos de los objetos", Toast.LENGTH_SHORT).show());
                }
            }

            @Override
            public void onFailure(Call<List<Item>> call, Throwable t) {
                MainActivity.this.runOnUiThread(()->Toast.makeText(MainActivity.this, "Error al sincronizar los datos de los objetos", Toast.LENGTH_SHORT).show());
            }
        });

    }


    private Boolean LoadLayout(Fragment frag){
        if (frag != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.nav_host_fragment_activity_main, frag)
                    .commit();
            return true;
        }
        return false;
    }
}