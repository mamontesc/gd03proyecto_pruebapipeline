package uex.asee.cajas.Entidades;

import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "objeto")
public class Objeto {
    @Ignore
    public static final String ITEM_SEP = System.getProperty("line.separator");

    @Ignore
    public final static String ID = "ID";

    @Ignore
    public final static String NAME = "name";

    @Ignore
    public final static String CATEGORIA = "categoria";

    @Ignore
    public final static String IMAGEN = "imagen";

    @Ignore
    public final static String DESCRIPTION = "description";

    @Ignore
    public final static String CAJA = "caja"; //SE DEBE CAMBIAR A TIPO CAJA

    @Ignore
    public final static String USERID = "userId";

    @PrimaryKey
    @NonNull
    private String id;

    @ColumnInfo(name = "name")
    private String name = new String();

    @ColumnInfo(name = "categoria")
    private String categoria = new String();

    @ColumnInfo(name = "imagen")
    private String imagen = new String();

    @ColumnInfo(name = "description")
    private String description = new String();

    @ColumnInfo(name = "caja")
    private String caja = new String();

    @ColumnInfo(name = "userId")
    private String userId = new String();

    @Ignore
    public Objeto(String name, String categoria,String imagen, String description, String caja, String userId) {
        this.name = name;
        this.categoria = categoria;
        this.imagen = imagen;
        this.description = description;
        this.caja = caja;
        this.userId = userId;
    }


    public Objeto(String id, String name, String categoria,String imagen, String description, String caja, String userId) {
        this.id = id;
        this.name = name;
        this.categoria = categoria;
        this.imagen = imagen;
        this.description = description;
        this.caja = caja;
        this.userId = userId;
    }

    @Ignore
    Objeto(Intent intent) {
        id = intent.getStringExtra(Objeto.ID);
        name = intent.getStringExtra(Objeto.NAME);
        categoria = intent.getStringExtra(Objeto.CATEGORIA);
        imagen = intent.getStringExtra(Objeto.IMAGEN);
        description = intent.getStringExtra(Objeto.DESCRIPTION);
        caja = intent.getStringExtra(Objeto.CAJA);
        userId = intent.getStringExtra(Objeto.USERID);
    }

    public String getId() { return id; }

    public void setId(String id) { this.id = id; }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public String getCategoria() { return categoria; }

    public void setCategoria(String categ) { this.categoria = categ; }

    public String getImagen() { return imagen; }

    public void setImagen(String img) { this.imagen = img; }

    public String getDescription() { return description; }

    public void setDescription(String desc) { this.description = desc; }

    public String getCaja() { return caja; }

    public void setCaja(String caja) { this.caja = caja; }

    public String getUserId() { return userId; }

    public void setUserId(String userId) { this.userId = userId; }

    public static void packageIntent (Intent intent, String name, String categoria, String description, String caja, String userId,String img) {
        intent.putExtra(Objeto.NAME, name);
        intent.putExtra(Objeto.CATEGORIA, categoria);
        intent.putExtra(Objeto.IMAGEN, img);
        intent.putExtra(Objeto.DESCRIPTION, description);
        intent.putExtra(Objeto.CAJA, caja);
        intent.putExtra(Objeto.USERID, userId);
    }

    public String toString() {
        return  id + ITEM_SEP + name + ITEM_SEP + categoria + ITEM_SEP + description + ITEM_SEP + caja + ITEM_SEP + userId+ITEM_SEP+imagen;
    }

    public String toLog() {
        return "ID: " + id + ITEM_SEP + " Nombre: " + name + ITEM_SEP + " Categoría: " + categoria + ITEM_SEP +
                " Descripción: " + description + ITEM_SEP + " Caja: " + caja + ITEM_SEP + " UserId: " + userId;
    }
}