package uex.asee.cajas.Adapters;

import java.util.List;


import uex.asee.cajas.Entidades.Objeto;

public interface OnItemsLoaderListener {
    public void onItemsLoaded(List<Objeto> items);
}
