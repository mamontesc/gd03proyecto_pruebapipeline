package uex.asee.cajas.Adapters;

import java.util.List;

import uex.asee.cajas.Entidades.Casa;
import uex.asee.cajas.Models.House;

public interface OnHousesLoaderListener {
    public void onHousesLoaded(List<Casa> houses);
}
