package uex.asee.cajas.Fragmentos;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;

import uex.asee.cajas.Actividades.CajaDetail;
import uex.asee.cajas.Actividades.Houses;
import uex.asee.cajas.Actividades.ItemDetail;
import uex.asee.cajas.Actividades.MainActivity;
import uex.asee.cajas.AppContainer;
import uex.asee.cajas.remote.AppExecutors;
import uex.asee.cajas.Entidades.Caja;
import uex.asee.cajas.Adapters.ItemAdapter;
import uex.asee.cajas.Adapters.ItemAdapterCaja;
import uex.asee.cajas.R;
import uex.asee.cajas.Roomdb.InventarioDatabase;
import uex.asee.cajas.ui.BoxesViewModel;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import java.util.concurrent.ThreadLocalRandom;

public class HomeFragment extends Fragment {
    private List<Caja> cajas;
    GridView gridView;
    ItemAdapter itemAdapter ;
    SwipeRefreshLayout swipeLayout;
    ItemAdapterCaja itemAdapterCaja ;
    BoxesViewModel mViewModel;

    public HomeFragment() {
        // Required empty public constructor
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        gridView = view.findViewById(R.id.grid3);

        itemAdapterCaja = new ItemAdapterCaja(getContext(), new ArrayList<>(), (caja,view1) -> {
            Intent intent = new Intent(getActivity(), CajaDetail.class);
            intent.putExtra(CajaDetail.CAJA_ID, caja.getId());
            startActivity(intent);
        });

        AppContainer appContainer = MainActivity.appContainer;

        mViewModel = new ViewModelProvider(this, appContainer.boxesfactory).get(BoxesViewModel.class);

        mViewModel.randomBox().observe(getViewLifecycleOwner(), boxes -> {
            System.out.println(boxes.size());
            if(boxes.size() ==0) {
                System.out.println("LA CAJA RANDOM ES NULA");
            }
            itemAdapterCaja.swap(boxes);
            gridView.setAdapter(itemAdapterCaja);
        });

        swipeLayout = view.findViewById(R.id.swiperefreshbox);
        Button localizaciones = view.findViewById(R.id.localizaciones);
        localizaciones.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), Houses.class);
                startActivity(intent);
            }
        });

        FloatingActionButton configButton = view.findViewById(R.id.ConfigButton);

        configButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getParentFragmentManager()
                        .beginTransaction()
                        .replace(R.id.nav_host_fragment_activity_main, new SettingsFragment())
                        .commit();
            }
        });
        return view;

    }

    @Override
    public void onStart() {
        super.onStart();
        //swipeRefresh();
    }

}