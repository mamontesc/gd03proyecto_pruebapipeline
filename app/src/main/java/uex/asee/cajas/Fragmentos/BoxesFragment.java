package uex.asee.cajas.Fragmentos;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import uex.asee.cajas.Actividades.CajaDetail;
import uex.asee.cajas.Actividades.ItemDetail;
import uex.asee.cajas.Actividades.Login;
import uex.asee.cajas.Actividades.MainActivity;
import uex.asee.cajas.Actividades.NewBox;
import uex.asee.cajas.Actividades.deleteCaja;
import uex.asee.cajas.AppContainer;
import uex.asee.cajas.remote.AppExecutors;
import uex.asee.cajas.Entidades.Caja;
import uex.asee.cajas.Interfaces.BoxAPI;
import uex.asee.cajas.Adapters.ItemAdapter;
import uex.asee.cajas.Adapters.ItemAdapterCaja;
import uex.asee.cajas.Models.Box;
import uex.asee.cajas.R;
import uex.asee.cajas.Roomdb.InventarioDatabase;
import uex.asee.cajas.remote.RetrofitClient;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uex.asee.cajas.ui.BoxesViewModel;
import uex.asee.cajas.ui.ItemsViewModel;

public class BoxesFragment extends Fragment {
    private boolean isFABOpen = false;
    private List<Caja> boxes;
    GridView gridView;
    ItemAdapterCaja itemAdapterCaja ;
    SwipeRefreshLayout swipeLayout;
    BoxesViewModel mViewModel;
    public BoxesFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_boxes, container, false);
        gridView = view.findViewById(R.id.grid3);
        //gridView.setAdapter(new ItemAdapter(view.getContext(), new ArrayList<>()));

        itemAdapterCaja = new ItemAdapterCaja(getContext(), new ArrayList<>(), (caja,view1) -> {
            Intent intent = new Intent(getActivity(), CajaDetail.class);
            intent.putExtra(CajaDetail.CAJA_ID, caja.getId());
            startActivity(intent);
        });

        AppContainer appContainer = MainActivity.appContainer;

        mViewModel = new ViewModelProvider(this, appContainer.boxesfactory).get(BoxesViewModel.class);

        mViewModel.getBoxes().observe(getViewLifecycleOwner(), boxes -> {
            itemAdapterCaja.swap(boxes);
        });



        gridView.setAdapter(itemAdapterCaja);
        swipeLayout = view.findViewById(R.id.swiperefreshbox);
        return view;

    }

    @Override
    public void onStart() {
        super.onStart();
        swipeLayout.post(()->{
            swipeLayout.setRefreshing(true);
            mViewModel.getBoxes().observe(getViewLifecycleOwner(), boxes -> {
                itemAdapterCaja.swap(boxes);
                swipeLayout.setRefreshing(false);
            });
        });

        swipeLayout.setOnRefreshListener(() -> {
            swipeLayout.setRefreshing(true);
            mViewModel.onRefresh();
            mViewModel.getBoxes().observe(getViewLifecycleOwner(), boxes -> {
                itemAdapterCaja.swap(boxes);
                swipeLayout.setRefreshing(false);
            });

        });
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        FloatingActionButton fab = view.findViewById(R.id.fab);
        FloatingActionButton fab1 = view.findViewById(R.id.fab1);
        FloatingActionButton fab2 = view.findViewById(R.id.fab2);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!isFABOpen){
                    showFABMenu(fab1,fab2);
                }else{
                    closeFABMenu(fab1,fab2);
                }
            }
        });
        fab1.setOnClickListener(view12 -> {
            Intent intent = new Intent(getActivity(), NewBox.class);
            startActivity(intent);
        });
        fab2.setOnClickListener(view1 -> {
            Intent intent = new Intent(getActivity(), deleteCaja.class);
            startActivity(intent);
        });

    }
    private void showFABMenu(FloatingActionButton fab1,FloatingActionButton fab2 ){
        isFABOpen=true;
        fab1.animate().translationY(-getResources().getDimension(R.dimen.standard_60));
        fab2.animate().translationY(-getResources().getDimension(R.dimen.standard_120));
    }

    private void closeFABMenu(FloatingActionButton fab1,FloatingActionButton fab2){
        isFABOpen=false;
        fab1.animate().translationY(0);
        fab2.animate().translationY(0);
    }
}
