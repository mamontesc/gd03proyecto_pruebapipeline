package uex.asee.cajas;

import static androidx.test.espresso.Espresso.onData;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.pressBack;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isRoot;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.rule.ActivityTestRule;

import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import uex.asee.cajas.Actividades.Login;

public class ModificarObjetoUITest {

    @Rule
    public ActivityScenarioRule<Login> mActivityRule =
            new ActivityScenarioRule<>(Login.class);

    @Test
    public void shouldModifyAnObject() {
        //Se selecciona el objeto que se quiere modificar
        onData(Matchers.anything()).inAdapterView(withId(R.id.grid)).atPosition(0).perform(click());
        //Se pulsa el botón para modificar el objeto
        onView(withId(R.id.button_edit_item)).perform(click());
        //Se modifican los datos del objeto
        onView(withId(R.id.eitem_name)).perform(typeText("OtroNombreDeMiObjeto"),closeSoftKeyboard());
        onView(withId(R.id.eitem_description)).perform(typeText("OtraDescripcionDeMiObjeto"),closeSoftKeyboard());
        //Se confirman los cambios de los datos
        onView(withId(R.id.action_confirm)).perform(click());
        //Se comprueba que, efectivamente, se ha cambiado el nombre y la descripción
        onView(withId(R.id.item_name)).check(matches(withText("OtroNombreDeMiObjeto")));
        onView(withId(R.id.item_desc)).check(matches(withText("OtraDescripcionDeMiObjeto")));
        //Se vuelve al fragmento de los objetos
        onView(isRoot()).perform(pressBack());
    }

    @Before
    public void LogInAndCreateObject() {
        String testingString = "UsuarioPrueba";
        String testEmail = "usuarioPrueba@gmail.com";
        //Se introducen los parámetros para iniciar sesión correctamente
        onView(withId(R.id.username)).perform(typeText(testEmail),closeSoftKeyboard());
        onView(withId(R.id.password)).perform(typeText(testingString),closeSoftKeyboard());
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //Se pulsa el botón de iniciar sesión
        onView(withId(R.id.loginButton)).perform(click());
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //Se accede al fragmento de los objetos
        onView(withId(R.id.items)).perform(click());
        //Se pulsa el botón flotante primero para desplegar los botones flotantes de crear o eliminar objetos
        onView(withId(R.id.fab)).perform(click());
        //Se pulsa el botón flotante de crear objeto
        onView(withId(R.id.fab1)).perform(click());
        //Se introducen los datos para crear un objeto
        onView(withId(R.id.nitem_name)).perform(typeText("miObjeto"),closeSoftKeyboard());
        onView(withId(R.id.nitem_description)).perform(typeText("descripcionDeMiObjeto"),closeSoftKeyboard());
        //Se selecciona que se quiere crear el objeto
        onView(withId(R.id.action_confirm)).perform(click());
    }

    @After
    public void WipeDataAndLogOut() {
        //Se vuelve a la pantalla principal
        onView(withId(R.id.home)).perform(click());
        //Se accede a la pantalla de ajustes
        onView(withId(R.id.ConfigButton)).perform(click());
        //Se selecciona que se quieren borrar todos los datos creados
        onView(withText("Reseteo de Fabrica")).perform(click());
        //Se pregunta si se está seguro de borrar los datos y se acepta
        onView(withText("Si")).perform(click());
        //Se pulsa el botón back para intentar cerrar la aplicación
        onView(isRoot()).perform(pressBack());
        //Se selecciona que se quiere salir de la aplicación
        onView(withText("Si")).perform(click());
    }
}
